const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
console.log("Start !")
module.exports = {
  entry: "./src/js/imports.js",
  /* entry: {main: "../src/js/index.js",vendor: "../src/js/libs/vendor.js"}, */
  module: {
    rules: [{
        test: /\.html$/,
        use: ["html-loader"]
      },
      {
        test: /\.(svg|png|jpg|gif)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "[name].[hash:8].[ext]",
            outputPath: "img"
          }
        }
      }
    ]
  }
};