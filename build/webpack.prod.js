const path = require("path");
const common = require("./webpack.common");
const merge = require("webpack-merge");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ZipPlugin = require('zip-webpack-plugin');


console.log("Production Mode !")
module.exports = merge(common, {
  mode: "production",
  output: {
    filename: "[name].[contentHash:8].bundle.js",
    path: path.resolve(__dirname, "../dist")
  },
  optimization: {
    minimizer: [
      new OptimizeCssAssetsPlugin({
        cleanOnceBeforeBuildPatterns: ['../dist/', '../zip/'],
      }),
      new TerserPlugin(),
      new HtmlWebpackPlugin({
        template: "./src/index.html",
        minify: {
          removeAttributeQuotes: true,
          collapseWhitespace: true,
          removeComments: true
        }
      })
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "[name].[contentHash:8].css"
    }),
    new CleanWebpackPlugin(),
    new FaviconsWebpackPlugin({
      logo: './src/images/favicons/favicon.png',
      prefix: './images-[hash:8]/favicons-[hash:8]/icon-[hash:8]',
      emitStats: false,
      statsFilename: 'iconstats-[hash].json',
      persistentCache: true,
      inject: true,
      background: '#fff',
      title: 'Webpack App',
      icons: {
        android: true,
        appleIcon: true,
        appleStartup: true,
        coast: false,
        favicons: false,
        firefox: true,
        opengraph: false,
        twitter: false,
        yandex: false,
        windows: true
      },
    }),
    new ZipPlugin({
      filename: 'app.zip',
      pathPrefix: 'relative/path',
      pathMapper: function (assetPath) {
        if (assetPath.endsWith('.png'))
          return path.join(path.dirname(assetPath), 'images', path.basename(assetPath));
        return assetPath;
      },
      fileOptions: {
        mtime: new Date(),
        mode: 0o100664,
        compress: true,
        forceZip64Format: false,
      },
      zipOptions: {
        forceZip64Format: false,
      },
    })
  ],
  module: {
    rules: [{
      test: /\.scss$/,
      use: [
        MiniCssExtractPlugin.loader, //3. Extract css into files
        "css-loader", //2. Turns css into commonjs
        "sass-loader" //1. Turns sass into css
      ]
    }]
  }
});